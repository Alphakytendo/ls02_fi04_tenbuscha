package angestelltenverwaltung;

public class Angestellter 
{
	
	//Dient als "Blaupause" f�r einen Angestelltenformular

	//Attribute
	 private String name;
	 private double gehalt;

	 //Methoden
	 
	 public void setName(String name)
	 {
	 this.name = name;
	 }
	 
	 
	 public String getName()
	 {
	 return this.name;
	 }
	 
	 
	 public void setGehalt(double gehalt)
	 {
	 this.gehalt = gehalt;
	 }
	 
	 
	 public double getGehalt()
	 {
	 return this.gehalt;
	 }
	

}
