
public class Ladung {

	private int menge;
	private String bezeichnung;
	
	

	
	/**Voraussetzung.: der String bezeichnung und der Integer menge m�ssen instanziiert sein,
	 * (Effekt.:) Ladung kann in RaumschiffTesten.java verwendet werden.
	 * @see RaumschiffTesten.java
	 * @param menge die Menge der Ladung
	 * @param bezeichnung die Bezeichnung der Ladung */
	
	public Ladung( String bezeichnung, int menge)
	{
		setmenge(menge);	// der Wert der Variable wird gesetzt
		setbezeichnung(bezeichnung);	// der Wert der Variable wird gesetzt
	}
	
	public void setmenge(int menge)
	{
		this.menge = menge;
	}
	
	public void setbezeichnung(String bezeichnung)
	{
		this.bezeichnung = bezeichnung;
	}
	
	public int getmenge (int menge)
	{
		return this.menge;
	}
	
	public String getbezeichnung(int menge)
	{
		return this.bezeichnung;
	}
}
