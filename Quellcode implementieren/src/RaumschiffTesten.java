import java.util.ArrayList;

public class RaumschiffTesten {

	/**Voraussetzung.: Die Klassen "Ladung" und "Raumschiff" müssen existieren,
	 * (Effekt.:) Die Methoden aus "Ladung und "Raumschiff" werden hier ausgeführt. */
	
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 100, 2);	// Die Werte der Klingonen wird hier gespeichert
		Ladung KLadung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung KLadung2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonen.addLadung(KLadung1); // Neue Ladungen werden hinzugefügt
		klingonen.addLadung(KLadung2);
		
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 1, 100, 100, 100, 100, 2); // Die Werte der Romulaner wird hier gespeichert
		Ladung RLadung1 = new Ladung("Borg-Schrott", 5);
		Ladung RLadung2 = new Ladung("Rote Materie", 2);
		Ladung RLadung3 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(RLadung1); // Neue Ladungen werden hinzugefügt
		romulaner.addLadung(RLadung2);
		romulaner.addLadung(RLadung3);
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 100, 50, 5);	// Die Werte der Vulkanier wird hier gespeichert
		Ladung VLadung1 = new Ladung( "Forschungssonde", 35);
		Ladung VLadung2 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(VLadung1);
		vulkanier.addLadung(VLadung2);
		
		klingonen.photonentorpedoAbschießen(romulaner); // Die Klingonen schießen die Romulaner ab
		romulaner.phaserkanonenAbschießen(klingonen);	// Die Romulaner schießen die Klingonen ab
		vulkanier.NachrichtAnAlle("Gewalt ist nicht logisch");	// Die Vulkanier schicken eine Nachricht an alle aus
		
		System.out.println("");
		klingonen.ZustanddesRaumschiffes(); // Der Zustand & Ladungsverzeichnis der Klingonen wird ausgegeben
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedoAbschießen(romulaner); //Die Klingonen schießen zweimal die Romulaner ab
		klingonen.photonentorpedoAbschießen(romulaner);
		
		System.out.println("");
		klingonen.ZustanddesRaumschiffes(); // Die Klingonen geben ihren Zustand & Ladungsverzeichnis aus
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("");
		romulaner.ZustanddesRaumschiffes(); // Die Romulaner geben ihren Zustand & Ladungsverzeichnis aus
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("");
		vulkanier.ZustanddesRaumschiffes(); // Die Vulkanier geben ihren Zustand & Ladungsverzeichnis aus
		vulkanier.ladungsverzeichnisAusgeben();
		
	}

}
