import java.util.ArrayList;

public class Raumschiff {

	private ArrayList<Ladung> ladungsverzeichnis;
	private ArrayList <String> broadcastKommunikator;
	private String schiffname;
	private int photonentorpedo;
	private int energieversorgung;
	private int schutzschilde;
	private int huelle;
	private int lebenserhaltungssysteme;
	private int androiden;
	private int phaserkanonen;
	
	/**Voraussetzung.: die ArrayList broadcastKommunikator,die ArrayList ladungsverzeichnis , der String schiffname, der int photonentorpedo, der int energieversorgung, der int schutzschilde, der int huelle, der int lebenserhaltungssysteme, der int androiden m�ssen instanziiert sein,
	 * (Effekt.:) Wird f�r die folgende Methoden ben�tigt & kann in RaumschiffTesten.java verwendet werden.
	 *  Die Werte f�r die Variablen werden gesetzt
	 *	@see RaumschiffTesten.java  */
	public Raumschiff(String schiffname, int photonentorpedo, int energieversorgung, int schutzschilde, int huelle, int lebenserhaltungssysteme, int androiden)
	{
		this.ladungsverzeichnis = new ArrayList<Ladung>();	// ArrayList wird deklariert
		this.broadcastKommunikator = new ArrayList<String>();	// ArrayList wird deklatiert
		setSchiffname(schiffname);		// der Wert der Variable wird gesetzt
		setphotonentorpedo(photonentorpedo);
		setenergieversorgung(energieversorgung);
		setschutzschilde(schutzschilde);
		sethuelle(huelle);
		setlebenserhaltungssysteme(lebenserhaltungssysteme);
		setandroiden(androiden);
		setphaserkanonen(phaserkanonen);
	}
	
	/**
	 * @deprecated Die Methode ist nicht essenziell damit das Programm l�uft */
	public Raumschiff()
	{
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	
	
	// Setter und Getter
	
	
	
	public ArrayList<Ladung> setladungsverzeichnis() 
	{
		return this.ladungsverzeichnis;		
	}
	
	public ArrayList<String> setbroadcastKommunikator() 
	{
		return this.broadcastKommunikator;		
	}

	public String setSchiffname(String schiffname) 
	{
		return this.schiffname;
	}
	
	public int setphotonentorpedo(int photonentorpedo) 
	{
		return this.photonentorpedo;
	}
	
	public int setenergieversorgung(int energieversorgung) 
	{
		return this.energieversorgung;
	}
	
	public int setschutzschilde(int schutzschilde) 
	{
		return this.schutzschilde;
	}
	
	public int sethuelle(int huelle) 
	{
		return this.huelle;
	}
	
	public int setlebenserhaltungssysteme(int lebenserhaltungssysteme) 
	{
		return this.lebenserhaltungssysteme;
	}
	
	public int setandroiden(int adroiden) 
	{
		return this.androiden;
	}
	
	public int setphaserkanonen(int phaserkanonen)
	{
		return this.phaserkanonen;
	}
	
	
	
	
	public ArrayList<Ladung> getladungsverzeichnis() 
	{
		return this.ladungsverzeichnis;		
	}
	
	public ArrayList<String> getbroadcastKommunikator() 
	{
		return this.broadcastKommunikator;		
	}
	
	public String getSchiffname(String schiffname) 
	{
		return this.schiffname;
	}
	
	public int getphotonentorpedo(int photonentorpedo) 
	{
		return this.photonentorpedo;
	}
	
	
	public int getenergieversorgung(int energieversorgung) 
	{
		return this.energieversorgung;
	}
	
	public int getschutzschilde(int schutzschilde) 
	{
		return this.schutzschilde;
	}
	
	public int gethuelle(int huelle) 
	{
		return this.huelle;
	}
	
	public int getlebenserhaltungssysteme(int lebenserhaltungssysteme) 
	{
		return this.lebenserhaltungssysteme;
	}
	
	public int getandroiden(int adroiden) 
	{
		return this.androiden;
	}
	
	public int getphaserkanonen(int phaserkanonen)
	{
		return this.phaserkanonen;
	}
	
	
	
	
	
	// Methoden
	
	
	/**Voraussetzung.: die ArrayList ladungsverzeichnis muss instanziiert und deklariert sein,
	 * (Effekt.:) Zum ladungsverzeichnis wird eine neue Ladung hinzugef�gt.
	 * @param neueLadung die neue Ladung, die hinzugef�gt wird */
	
	public void addLadung(Ladung neueLadung)
	{
		this.ladungsverzeichnis.add(neueLadung); // Zum ladungsverzeichnis wird eine neue Ladung hinzugef�gt.
	}
	
	/**Voraussetzung.: die Getter (und Setter) m�ssen instanziiert und deklariert sein,
	 * (Effekt.:) die Werte der Variablen der Objekte werden genommen und ausgegeben. */
	
	public void ZustanddesRaumschiffes() 
	{
		System.out.println("Der Zustand des Schiffes wird ausgegeben:");
		System.out.println("Der Schiffsname lautet:"+this.getSchiffname(schiffname));	// Der Wert der Variable wird genommen und ausgegeben
		System.out.println("Die Energieversorgung:"+this.getenergieversorgung(energieversorgung));
		System.out.println("Die Schutzschilde:"+this.getschutzschilde(schutzschilde));
		System.out.println("Die Lebenserhaltungssysteme:"+this.getlebenserhaltungssysteme(lebenserhaltungssysteme));
		System.out.println("Die Huelle:"+this.gethuelle(huelle));
		System.out.println("Die Photonentorpedos:"+this.getphotonentorpedo(photonentorpedo));
		System.out.println("Die Androiden:"+this.getandroiden(androiden));
		
	}
	
	/**Voraussetzung.: die Getter (und Setter) m�ssen instanziiert und deklariert sein,
	 * (Effekt.:) Gibt den Ladungsverzeichnis aus. */
	
	public void ladungsverzeichnisAusgeben()
	{
		System.out.println("Ladungen:");
		for(Ladung l : this.ladungsverzeichnis)
		{
			System.out.println("Ladungs Bezeichnung:"+ l.getbezeichnung(0));	// Der Wert der Variable wird genommen und ausgegeben
			System.out.println("Ladungs Menge:"+l.getmenge(0));
		}
	}
	
	/**Voraussetzung.: die Getter (und Setter) m�ssen instanziiert und deklariert sein,
	 * (Effekt.:) photonentorpedos k�nnen abgeschossen werden. 
	 * Wenn es keine photonentorpedos gibt wird "Click" und eine Methode ausgef�hrt, andernfalls wird dem Wert der Variable des Objektes 1 abgezogen */
	
	public void photonentorpedoAbschie�en(Raumschiff raumschiff)
	{
		if(this.getphotonentorpedo(photonentorpedo) == 0)	// falls photonentorpedos 0 betr�gt, wird "Click" ausgegeben und eine Methode wird ausgef�hrt
		{
			System.out.println("-=*Click*=-");
			NachrichtAnAlle(schiffname);
		} 
		else	// falls nicht, wird photonentorpedos 1 abgezogen
		{
			this.setphotonentorpedo(this.getphotonentorpedo(photonentorpedo)-1);
		}
	}

	/**Voraussetzung.: die Getter (und Setter) m�ssen instanziiert und deklariert sein,
	 * (Effekt.:) phaserkanonen k�nnen abgeschossen werden. 
	 * Wenn die phaserkanonen unter 50% sind wird "Click" und eine Methode ausgef�hrt, andernfalls wird dem Wert der Variable des Objektes 50% abgezogen und es wird eine Methode ausgegeben. */
	
	public void phaserkanonenAbschie�en(Raumschiff raumschiff)	
	{
		if(this.getphaserkanonen(phaserkanonen) > 50) // falls phaserkanonen unter 50% betr�gt, wird "Click" ausgegeben und eine Methode wird ausgef�hrt
		{
			System.out.println("-=*Click*=-");
			NachrichtAnAlle(schiffname);
		} 
		else	// falls nicht, wird phaserkanonen 50% abgezogen und eine Methode wird ausgef�hrt, die einen Treffer vermerkt
		{
			this.setphaserkanonen(this.getphaserkanonen(phaserkanonen)- 50);
			System.out.println("Phaserkanone abgeschossen");
			Treffervermerken(raumschiff);
		}
	}

	/**Voraussetzung.: die Getter (und Setter) m�ssen instanziiert und deklariert sein,
	 * (Effekt.:) Die Nachricht wird in der Konsole ausgegeben.  
	 * @see phaserkanonenAbschie�en */
	
	public void Treffervermerken(Raumschiff raumschiff)
	{
		System.out.println( this.getSchiffname(schiffname)+"wurde getroffen!");	// Treffer wird vermerkt
	}

	/**Voraussetzung.: die Getter (und Setter) m�ssen instanziiert und deklariert sein,
	 * (Effekt.:) Die Nachricht wird in der Konsole ausgegeben.  */
	
	public void NachrichtAnAlle(String string)
	{
		System.out.println("Nachricht an ALLE!");	// Nachricht an alle wird ausgegeben
	}
	
}






